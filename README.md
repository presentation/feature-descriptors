## Abstract

What do we do to differentiate one thing from another? What do we do to create a representation of our world in such a way that we can embrace the variations of one type of thing, but still manage to differentiate that thing from others? In short, we extract the essence of the objects and model them.

We call the essential characteristics feature descriptors. In general, we can extract them from any type of data and used them to create models. In this talk, we explore some type of feature descriptors, based on visual cues, to define objects and recognize them in images and videos. Also, we will discuss about some venues in which we can apply the lessons from vision and machine learning to some other domains.

