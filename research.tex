\documentclass{beamer}
\usepackage[utf8]{inputenc}

% For the .tex files that uses standalone
\usepackage{standalone}

% My setup
\usetheme{adn}
\setlogosz{2cm}

% Automatic convertion to pdf
%\usepackage{svg}

%\setsvg{svgpath=./images/}
\graphicspath{{./images/}}

\setbeamertemplate{title page}[transparent][]
\setbackgroundimage{./images/gdd}


% change the blocks background to white
\setbeamercolor{block body}{bg=white}
% make the overlay to be transparent
\setbeamercovered{transparent}

% For each section add the ToC
\AtBeginSection[]{
\begin{frame}
   \frametitle{Agenda}
   \tableofcontents[currentsection]
\end{frame}
}

\usepackage[plain]{algorithm}% plain removes the rules from the algorithm
\usepackage{algpseudocode}%from algorithimicx

% flow diagram settings
\usepackage{tikz}
\usetikzlibrary{
%  babel,
  positioning,
  calc,
  fadings,
  fontawesome
}
\usepackage{pgfplots}

% Source code
\usepackage{codetools}

% For strikeout
\usepackage{soul}

%\hypersetup{urlcolor=black}

\begin{document}
% Settings
\title{Feature Descriptors}
\subtitle{Image and Video (and more)}

\author[Adin]{Adín Ramírez Rivera\\%
  {\small%
    \fa{fas-paper-plane} \texttt{adin@ic.unicamp.br} \qquad
    \fa{fas-globe} \texttt{ic.unicamp.br/\tildett adin} \qquad
    \fa{fab-twitter} \texttt{@adn\_twitts}%
  }}

\course{}
\date{September 2016\\%
  \tiny
  \href{https://research.googleblog.com/2015/07/deepdream-code-example-for-visualizing.html}{\color{white!90!black}https://research.googleblog.com/2015/07/deepdream-code-example-for-visualizing.html}
}


\begin{frame}
\titlepage
\end{frame}

\section{Introduction}

\begin{frame}{Lets sync expectations}{Preface}
  \begin{itemize}
    \item What this talk \alert{isn't}
      \begin{itemize}
        \item Technical
        \item Deep descriptions of the subjects
      \end{itemize}
    \item At the end of the talk
      \begin{itemize}
        \item You won't be able to do CV
        \item You won't be coding the algorithms
        \item \alert{But!}
        \item You will know the overall idea
        \item You will understand the big picture
      \end{itemize}
    \item If you are interested, \alert{we can discuss details later!}
  \end{itemize}
\end{frame}

\subsection*{Who am I?}

\begin{frame}{-- adin}
\begin{picture}(0,0)
\put(0,-100){\includegraphics[width=\linewidth]{map}}
\put(0,70){%
  \begin{minipage}{0.5\linewidth}
  \begin{itemize}
  \item Guatemalan
  \item B.Eng. Computer Science and Systems, USAC
  \end{itemize}
  \end{minipage}}
\put(190,70){%
  \begin{minipage}{0.5\linewidth}
  \begin{itemize}
  \item M.Sc. Computer Vision,
  \item Ph.D. Computer Vision, KHU, South Korea
  \end{itemize}
  \end{minipage}}
\put(-20,-120){%
  \begin{minipage}{0.4\linewidth}
  \begin{itemize}
  \item Assistant Professor, UDP, Chile
  \end{itemize}
  \end{minipage}}
\put(100,-120){%
  \begin{minipage}{0.4\linewidth}
  \begin{itemize}
  \item Assistant Professor, UNICAMP, Brazil
  \end{itemize}
  \end{minipage}}
\end{picture}
\end{frame}

\subsection*{What is Computer Vision?}

\begin{frame}[fragile]{Computer Vision and Related Fields}
  \begin{center}
    \includegraphics[width=.9\linewidth]{cv-overview}
%    \includesvg[width=.9\linewidth]{cv-overview}
  \end{center}
\end{frame}

\begin{frame}
  \begin{center}
    \includegraphics<1>[height=.8\textheight, interpolate]{tasks}
    \includegraphics<2>[width=\textwidth, interpolate]{parkorbird}
  \end{center}
  
  \only<1>{\nomarkfootnote{\url{http://xkcd.com/1425/}}}
  \only<2>{\nomarkfootnote{\url{http://parkorbird.flickr.com/}}}
\end{frame}



\begin{frame}{Brief History of Computer Vision}
  \begin{picture}(0,0)
  \put(160,-138){\includegraphics[width=6cm, interpolate]{robot}}
  \put(0,0){%
    \begin{minipage}{\linewidth}
    \begin{itemize}
    \item 1960: interpretation of synthetic worlds
    \item \alert{1966: Minsky assigns a homework to a student, plug the camera to the computer and make it interpret what it is seeing}
    \item 1970: progress interpreting selected images
    \item 1980: ANNs come and go, there is a tendency towards geometry and math
    \item 1990: face recognition and statistical analysis
    \item 2000: broader recognition, several databases appear, and we start processing videos
    \item 2010: deep learning
    \item 2030: robot revolution?
    \end{itemize}
    \end{minipage}
  }
  \nomarkfootnote{\url{https://what-if.xkcd.com/5/}}
  \end{picture}
\end{frame}

\newlength{\sz}
\setlength{\sz}{0.33\linewidth}
\begin{frame}{Why does it matters?}
\centering
\begin{tabular}{@{}c@{ }@{}c@{ }@{}c@{}}
\includegraphics[width=\sz]{google-self-driving-car}&
\includegraphics[width=\sz]{ct-reconstruction}&
\includegraphics[width=\sz]{surveillance}\\
Self driving & Health & Surveillance\\
\includegraphics[width=\sz]{hospital-robot}&
\includegraphics[width=\sz]{kinect-baseball}&
\includegraphics[width=\sz]{image-search}\\
  Security & Fun & Search\\
\end{tabular}
\end{frame}


\subsection*{Facial Expression}
\begin{frame}{Smart Environments}

\begin{block}{Mood-aware rooms}
  \centering
  \includegraphics[width=0.5\linewidth]{room1}%
  \includegraphics[width=0.5\linewidth]{room2}
\end{block}
\vspace{-15pt}
\begin{columns}[T]

  \begin{column}{0.45\textwidth}
    \begin{block}{Smart Museums}
      \centering
      \includegraphics[width=0.8\linewidth]{museum}
    \end{block}
  \end{column}
  
  \begin{column}{0.45\textwidth}
    \begin{block}{Smart TV}
      \centering
      \includegraphics[width=0.8\linewidth]{smart-tv}
    \end{block}
  \end{column}
\end{columns}

\end{frame}

\subsection*{Challenges of Facial Analysis}
\begin{frame}{Degrees of Freedom}
  \vspace{-10pt}
  \begin{columns}[T]
    \begin{column}{0.42\textwidth}
      \begin{block}{Race}
        \includegraphics[width=\linewidth]{race}
      \end{block}
    \end{column}
    \begin{column}{0.45\textwidth}
      \begin{block}{Age}
        \includegraphics[width=\linewidth]{aging}
      \end{block}
      \begin{block}{Illumination}
        \includegraphics[width=\linewidth]{illumination}
      \end{block}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Degrees of Freedom}
  \vspace{-5pt}
  \begin{block}{Environment}
    \includegraphics[width=\linewidth]{environment}
  \end{block}
  \vspace{-15pt}
  \begin{columns}[T]
    \begin{column}{0.4\textwidth}
      \begin{block}{Pose and Orientation}
        \centering
        \includegraphics[width=0.8\linewidth]{pose}
      \end{block}
    \end{column}
    \begin{column}{0.4\textwidth}
      \begin{block}{Technology}
        \centering
        \includegraphics[width=0.75\linewidth]{technological}
      \end{block}
    \end{column}
  \end{columns}
\end{frame}

\subsection*{More research}
\begin{frame}{And more related research (but later)}{\st{xkcd,} the world and research are in superposition too: sad and wonderful}
  \vspace{-20pt}
  \begin{center}
    \includegraphics[height=.8\textheight]{click_and_drag}
  \end{center}
  \vspace{-20pt}
  \nomarkfootnote{\url{http://xkcd.com/1110/}}
\end{frame}

\section{Feature Descriptor}

\subsection*{What is essential?}

\begin{frame}{What is this?}
  \begin{center}
    \only<+>{\resizebox{!}{.8\textheight}{\input{./images/house}}}
    \only<+>{\resizebox{!}{.8\textheight}{\input{./images/church}}}
    \only<+>{\resizebox{\linewidth}{!}{\input{./images/car}}}
  \end{center}
\end{frame}

\doublecolumnframe[title={Why are we able to tell what it is?}]{%
  \begin{itemize}
    \item Model
    \item \alert{Structure}
      \begin{itemize}
        \item Linear
        \item Non linear
        \item Recursive
      \end{itemize}
    \item Parts $\rightarrow$ Whole
  \end{itemize}
  
  \begin{definition}
    A feature is an \textbf{essential} characteristic of the ``thing'' we are interested in.
  \end{definition}
}{
  \resizebox{\linewidth}{!}{\input{./images/model}}
}

\subsection*{Image Descriptors}

\begin{frame}{\only<2>{{\color{secondColor!50}Real}} Image Descriptors}
\centering
\begin{tikzpicture}
\tikzfading[name=fade out, inner color=transparent!0, outer color=transparent!100]

\begin{scope}[transparency group]
\path [scope fading= fade out] (-2.1,-1.5) rectangle (2.4,2.3);
\node[] (fp) at (0,0) {\includegraphics[width=0.4\linewidth]{fingerprint}};
\end{scope}
\only<2>{
\begin{axis}[at ={($(fp.north west)+(-1cm,-1.cm)$)},
  width=3.5cm, height=2.5cm,
  ticks = none,
]
\addplot [
  fill=orange!75,
  draw=orange!50!black,
  ybar,
  bar width=6pt,
] coordinates {(0,3) (1,2) (2,4) (3,1) (4,2) (5,2) (6,8)};
\end{axis}
}

\node[above right=-.5cm and .5cm of fp] (2) {\includegraphics[width=0.4\linewidth]{udp-2}};
\node[below right=-1.2cm and .5cm of fp] (3) {\includegraphics[width=0.4\linewidth]{udp-3}};
\node[above right=-3.4cm and 1.5cm of fp] (1) {\includegraphics[width=0.3\linewidth]{udp-1}};

\draw[->, ultra thick, red] (1.west) -- ($(fp.east)+(0pt,9pt)$);
\draw[->, ultra thick, red] (2) -- ($(fp.east)+(0pt,14pt)$);
\draw[->, ultra thick, red] (3) -- ($(fp.east)+(0pt,5pt)$);

\node[left=0cm of 2] {Similar images};
\node[below=-.8cm of fp] {\only<1>{Same}\only<2>{\alert{Similar}} descriptor};
\only<2>{
  \coordinate (h) at ($(fp.north west)+(0.7cm,-1.cm)$);
  \node[below=0cm of h, text width=2.5cm] (lbl) {Vector};
  \draw[->, ultra thick, red] ($(fp.west)+(1.cm,0cm)$) -| ($(lbl.south west)+(.8cm,0cm)$);
}
\end{tikzpicture}
\end{frame}

\begin{frame}{Appearance-based Methods}
  \includegraphics[width=\linewidth]{dense-descriptor}
  \begin{itemize}
    \item Performance depends on the dense descriptor used
    \item Image descriptors
    \begin{itemize}
      \item Describe local \alert{micro-patrones} (local texture, color, shape patterns, etc.)
      \item Improves the recognition performance
      \item Examples: Gabor, Gradients, LBP, LDP, \alert{directional numbers}
    \end{itemize}
  \end{itemize}
\end{frame}

\doublecolumnframe[title={Description and Recognition of Micro Patterns}, subtitle={Our problem}, width=0.6\linewidth]{%
  \centering
  \begin{block}{Micro Patterns}
    Patterns that show up in small neighborhoods in images ($3 \times 3$, $5 \times 5$, etc.)
  \end{block}
  \begin{exampleblock}{Examples}
    \footnotesize
    \begin{itemize}
      \item Lines, corners, or complex textures
      \item Build local textures, color, shape patterns
    \end{itemize}
  \end{exampleblock}
  \includegraphics[width=0.8\linewidth]{micro-pattern}
}{%
  Several applications depend on the description of what they are processing
  \begin{itemize}
    \item Bird vs. park recognition
    \item Object recognition
    \item Content-based image retrieval
    \item \alert{Face analysis}
    \item Texture recognition
    \item \dots
  \end{itemize}
  
  Then we feed them into a machine learning algorithm {\footnotesize (that needs another talk)}
}

\begin{frame}{Video Descriptors}{Simple extension}
\includegraphics[width=\linewidth]{single-volume}
\end{frame}

\doublecolumnframe[title={Representations}, width=.7\linewidth]{%
  \begin{itemize}
    \item Histograms (counting and placing in vector)
      \begin{itemize}
        \item Easy
        \item Not much structure
      \end{itemize}
    \item Bag of words (features)
      \begin{itemize}
        \item Easy
        \item Some structure
      \end{itemize}
    \item Graphs
      \begin{itemize}
        \item Intermediate
        \item More structure
      \end{itemize}
    \item More complex structures
      \begin{itemize}
        \item Difficulty depends on the task
        \item Much more structure and content
      \end{itemize}
  \end{itemize}
}{%
  \resizebox{!}{0.8\textheight}{\input{./images/descriptors}}
}

\section{Example}
\subsection*{DNG}
\begin{frame}{Spatiotemporal Directional Number Transitional
Graph}
\hspace*{-28pt}
\includegraphics[width=1.16\linewidth]{code-ex}
\nomarkfootnote{Ramírez and Chae, Spatiotemporal Directional Number Transitional
Graph for Dynamic Texture Recognition, PAMI 2015}
\end{frame}

\begin{frame}{Directional Number Graph}
  \begin{columns}[T]
    \begin{column}{0.4\textwidth}
      \begin{block}{Graph}
        \centering
        \resizebox{\linewidth}{!}{%
        \begin{tikzpicture}[baseline=0pt,label/.style={font=\tiny,fill=white,rectangle,rounded corners,inner sep=1pt]}]%
        \tikzstyle{every loop}=[looseness=5]
        %
        \clip (-2.7,-2.4) rectangle (2.8,2.4);
        \def \n {6}%
        \def \radius {1.5cm}%
        \def \margin {8} % margin in angles, depends on the radius
        \def \names{{"v_0","v_1","v_2","v_3","\dots","v_{n-1}"}}%
        \def \weights{{"0","1","2","3","","n"}}%
        \pgfmathparse{\n-1} \let\nn\pgfmathresult%
        % draw the nodes
        \foreach \s in {0,...,\nn} {%
          \pgfmathparse{\names[\s]} \let\nd\pgfmathresult%
          \node[draw, circle, font=\tiny, minimum size=1cm] (n-\s) at ({360/\n * (\s)}:\radius) {$\nd$};%
        }%
        % draw the edges for all the nodes except the first
        \foreach \na in {0,...,\nn} {%
          \pgfmathparse{360/\n * \na} \let\theta\pgfmathresult%
          \pgfmathparse{120-90+\theta} \let\beta\pgfmathresult%
          \pgfmathparse{30-90+\pgfmathresult} \let\alpha\pgfmathresult%
          \pgfmathparse{\weights[\na]} \let\la\pgfmathresult%
          \path (n-\na) edge[->,in=\beta,out=\alpha,loop] (n-\na);%
          \pgfmathparse{int(\na+1)} \let\ns\pgfmathresult%
          \ifnum \pgfmathresult < \n%
            \foreach \nb in {\ns,...,\nn} {%
              \path (n-\na) edge[->,bend right=5] (n-\nb) edge[<-,bend left=5] (n-\nb);%
            }%
          \fi%
        }%
        % draw the first and label them
        \path (n-0) edge[in=30,out=-30,loop,draw opacity=0] node[label,pos=0.5] {$\omega_{0,0}$} (n-0);
        \path (n-0) edge[->,bend right=5,draw opacity=0,right] node[label,pos=0.8] {$\omega_{0,1}$}(n-1) edge[<-,bend left=5,draw opacity=0,left] node[label,pos=0.2] {$\omega_{1,0}$} (n-1);
        \end{tikzpicture}%
        }%end resize
      \end{block}

    \end{column}
    \begin{column}{0.6\textwidth}
      \begin{itemize}
        \item Transition graph $G$ 
        \item with codes as vertices $V = \{v_1, \dots, v_n\}$, 
        \item directed edges $E = \{ (v_j,v_k) \mid \forall\, v_j, v_k \in V \}$, y
        \item weights $W = \{ w_{j,k} \mid \forall\,j,k \}$
        \item Implemented with the adjacency matrix $G$
      \end{itemize}
    \end{column}
  \end{columns}
  \begin{exampleblock}{Learn the weights}
    \begin{align*}
    w(v_{j},v_{k}) &= \sum_{(x,y,t) \in I} {\delta_{j,k} (i_{x,y,t},i_{x,y,t+1})}
    \end{align*}
  \end{exampleblock}

\end{frame}

\begin{frame}{Filters 3D}
  \begin{columns}
    \begin{column}{0.45\textwidth}
      \begin{itemize}
        \item We proposed a new 3D filter to extract principal directions on the spatio-temporal domain
        \item Rotate the mask on 9 directions to generate a \emph{compass mask}
        \scriptsize
         \begin{align*}
         M_1^{z=1} &= \begin{bmatrix}
         1 & 2 & 1\\
         2 & 4 & 2\\
         1 & 2 & 1
         \end{bmatrix},\\
         M_1^{z=2} &= \begin{bmatrix}
         0 & 0 & 0\\
         0 & 0 & 0\\
         0 & 0 & 0
         \end{bmatrix},\\
         M_1^{z=3} &= \begin{bmatrix}
         -1 & -2 & -1\\
         -2 & -4 & -2\\
         -1 & -2 & -1
         \end{bmatrix}
         \end{align*}
       \end{itemize}
    \end{column}
    \begin{column}{0.45\textwidth}
      \begin{block}{3D masks}
        \centering
        \includegraphics[width=0.8\linewidth]{sym-planes}
      \end{block}
      \begin{exampleblock}{Example}
        \includegraphics[width=\linewidth]{3d-mask}
      \end{exampleblock}
    \end{column}
  \end{columns}
\end{frame}

\subsection*{CNN}

\begin{frame}{CNN Idea}
  \hspace*{-25pt}
  \includegraphics[width=1.2\linewidth]{cnn-kernels}
  \nomarkfootnote{\url{https://devblogs.nvidia.com/parallelforall/accelerate-machine-learning-cudnn-deep-neural-network-library/}}
\end{frame}

\begin{frame}{CNN Features}
  \hspace*{-25pt}
  \includegraphics[width=1.15\linewidth]{cnn-features}
  \nomarkfootnote{\url{ http://www.rsipvision.com/wp-content/uploads/2015/04/Slide6.png}}
\end{frame}

\begin{frame}{Features for other than classify}{Generative Models}
  \resizebox{\linewidth}{!}{\includegraphics{cnn-style}}
  \nomarkfootnote{A Neural Algorithm of Artistic Style. \url{http://arxiv.org/abs/1508.06576}}
\end{frame}

\subsection*{And more}

\begin{frame}{And more\dots}{Ideas and work in progress}
  \begin{itemize}
    \item Extract features from matrices using CV algorithms and process with ML
      \begin{itemize}
        \item Can an ML algorithm learn to permute matrices? (we are still trying)
        \item Can an ML algorithm learn to compress matrices? and do operations on the new representation?
      \end{itemize}
    \item Data fusion
      \begin{itemize}
        \item Fuse information from wireless sensors and predict stuff
        \item IoT meets ML with the CV know-how
      \end{itemize}
    \item Few examples for learning
      \begin{itemize}
        \item Humans learn with few samples
        \item Why machines need thousands of thousands?
      \end{itemize}
  \end{itemize}
\end{frame}

\section{Conclusions}
\begin{frame}{Conclusions}
\begin{itemize}
\item Different sides but related
  \begin{itemize}
    \item Computer Vision creates models from images
    \item Image Processing creates images from images
    \item Machine Vision makes action from images
  \end{itemize}
\item \alert{All sides are powered by machine learning}
\item Feature definition and extraction as the bottom of a processing pipeline
\item We need smarter and more robust features
\item Garbage in, garbage out
\item Ideas from one field can propagate to another
  \begin{itemize}
    \item Cool things to do
    \item Exiting times to jump into the train
    \item AI/CV/ML startups have been selling for \alert{a lot}
  \end{itemize}
\end{itemize}
\end{frame}

\section*{QA}
\begin{frame}{}
\vspace*{50pt}
\begin{picture}(0,0)
\put(0,0){%
  \begin{beamercolorbox}[center,shadow=true,rounded=true,]{block title}
  \Huge Questions?
  \end{beamercolorbox} 
}
\put(208,-94){\includegraphics[width=4cm]{question}}
\end{picture}

\vspace*{100pt}

  Interested? Want the detailed version? \textbf{Lets chat!}
  
  How to contact me:
  
\tikz[color=secondColor]\pic{fas-paper-plane}; \texttt{adin@ic.unicamp.br}

\tikz[color=secondColor]\pic{fas-globe}; {\hypersetup{urlcolor=black}\url{https://ic.unicamp.br/~adin}}

\tikz[color=secondColor]\pic{fab-twitter}; \texttt{@adn\_twitts}%

\nomarkfootnote{\url{http://xkcd.com/1448/}}
\end{frame} 

\end{document}